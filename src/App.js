import React from 'react';
import logo from './logo.svg';
import './App.css';
import Attendance from "./Attendance";

function App() {
  return (
    <div className="App">
      <h2>Hello VAMK</h2>
      <Attendance title="Attendance codes for Timo's course"/>
    </div>
  );
}

export default App;
